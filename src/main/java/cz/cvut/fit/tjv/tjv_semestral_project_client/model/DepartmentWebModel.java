package cz.cvut.fit.tjv.tjv_semestral_project_client.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class DepartmentWebModel extends DepartmentDto {
    public final Set<FiretruckDto> firetrucks;
    public final Set<DispatchDto> dispatches;
    public final Set<FirefighterDto> firefighters;

    public DepartmentWebModel() {
        firetrucks = new HashSet<>();
        dispatches = new HashSet<>();
        firefighters = new HashSet<>();
    }

    public DepartmentWebModel(DepartmentDto dto) {
        super(dto);
        firetrucks = new HashSet<>();
        dispatches = new HashSet<>();
        firefighters = new HashSet<>();
    }

    public DepartmentWebModel(DepartmentDto dto, Set<DispatchDto> dispatches,
                              Set<FirefighterDto> firefighters, Set<FiretruckDto> firetrucks) {
        super(dto);
        this.dispatches = dispatches;
        this.firefighters = firefighters;
        this.firetrucks = firetrucks;
    }

    public Collection<FiretruckDto> getFiretrucks() {
        return firetrucks;
    }

    public Collection<DispatchDto> getDispatches() {
        return dispatches;
    }

    public Collection<FirefighterDto> getFirefighters() {
        return firefighters;
    }
}
