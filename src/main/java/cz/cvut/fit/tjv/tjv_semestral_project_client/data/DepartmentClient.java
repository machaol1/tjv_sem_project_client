package cz.cvut.fit.tjv.tjv_semestral_project_client.data;

import cz.cvut.fit.tjv.tjv_semestral_project_client.model.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.Set;
import java.util.stream.Collectors;

@Component
public class DepartmentClient extends AbstractClient<DepartmentWebModel, DepartmentDto, Long> {
    protected final String DIS_URI = ONE_URI + "/dispatch";
    protected final String FF_URI = ONE_URI + "/firefighter";
    protected final String FT_URI = ONE_URI + "/firetruck";

    public DepartmentClient (@Value("${backend_url}") String backendUrl) {
        super(backendUrl, "/department", DepartmentWebModel.class);
    }

    @Override
    protected Mono<DepartmentWebModel> readById(Long id) {
        Mono<DepartmentDto> departmentDtoMono = webClient.get()
                .uri(ONE_URI, id)
                .retrieve()
                .bodyToMono(DepartmentDto.class);

        Mono<Set<DispatchDto>> dispatchDto = webClient.get()
                .uri(DIS_URI, id)
                .retrieve()
                .bodyToFlux(DispatchDto.class)
                .collect(Collectors.toSet()); // Collect the Flux into a Set

        Mono<Set<FirefighterDto>> firefighterDto = webClient.get()
                .uri(FF_URI, id)
                .retrieve()
                .bodyToFlux(FirefighterDto.class)
                .collect(Collectors.toSet());

        Mono<Set<FiretruckDto>> firetruckDto = webClient.get()
                .uri(FT_URI, id)
                .retrieve()
                .bodyToFlux(FiretruckDto.class)
                .collect(Collectors.toSet());

        // parallel version - might be a faster but not noticeable on a small sample size
        return departmentDtoMono.flatMap(department ->
                Mono.zip(dispatchDto, firefighterDto, firetruckDto)
                        .map(tuple -> new DepartmentWebModel(department, tuple.getT1(), tuple.getT2(), tuple.getT3()))
        );

//        return Mono.zip(departmentDtoMono, dispatchDto, firefighterDto, firetruckDto)
//                .map(tuple -> new DepartmentWebModel(tuple.getT1(), tuple.getT2(), tuple.getT3(), tuple.getT4()));
    }

    @Override
    public DepartmentWebModel newWM() {
        return new DepartmentWebModel();
    }

    @Override
    public DepartmentWebModel newWM(DepartmentDto dto) {
        return new DepartmentWebModel(dto);
    }
}
