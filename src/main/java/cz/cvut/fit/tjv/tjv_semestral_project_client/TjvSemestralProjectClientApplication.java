package cz.cvut.fit.tjv.tjv_semestral_project_client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TjvSemestralProjectClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(TjvSemestralProjectClientApplication.class, args);
	}

}
