package cz.cvut.fit.tjv.tjv_semestral_project_client.data;

import cz.cvut.fit.tjv.tjv_semestral_project_client.model.DepartmentDto;
import cz.cvut.fit.tjv.tjv_semestral_project_client.model.FiretruckDto;
import cz.cvut.fit.tjv.tjv_semestral_project_client.model.FiretruckWebModel;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class FiretruckClient extends AbstractClient<FiretruckWebModel, FiretruckDto, Long> {
    protected static final String DEP_URI = ONE_URI + "/department";
    protected final WebClient departmentWebClient;

    public FiretruckClient (@Value("${backend_url}") String backendUrl) {
        super(backendUrl, "/firetruck", FiretruckWebModel.class);
        departmentWebClient = WebClient.create(backendUrl + "/department");
    }

    @Override
    public Mono<FiretruckWebModel> create(FiretruckWebModel wm) {
        return departmentWebClient.post() // HTTP POST
                .uri("/{depId}/firetruck", wm.departmentId)
                .contentType(MediaType.APPLICATION_JSON) // set HTTP header
                .bodyValue(wm) // POST data
                .retrieve() // request specification finished
                .bodyToMono(FiretruckWebModel.class) // interpret response body as one element using WM class
                ;
    }

    @Override
    public Mono<Void> edit(Long id, FiretruckWebModel firetruckWebModel) {
        return webClient.put() // HTTP PUT
                .uri(DEP_URI + "/{FFId}", id, firetruckWebModel.departmentId) // update firefighter department
                .retrieve() // request specification finished
                .toBodilessEntity()
                .then(Mono.defer(() -> super.edit(id, firetruckWebModel))) // update firefighter
                ;
    }

    @Override
    protected Mono<FiretruckWebModel> readById(Long id) {
        return super.readById(id).flatMap(this::readDepartment);
    }

    @Override
    protected Flux<FiretruckWebModel> readAll() {
        return super.readAll().flatMap(this::readDepartment);
    }

    private Mono<FiretruckWebModel> readDepartment(FiretruckWebModel firetruckWebModel) {
        return webClient.get() // /object
                .uri(DEP_URI, firetruckWebModel.id) // /object/{id}
                .retrieve() // read
                .bodyToMono(DepartmentDto.class) // interpret response body as WM using WM class
                .map(departmentDto -> {
                    firetruckWebModel.setDepartment(departmentDto);
                    return firetruckWebModel;
                })
                ;
    }

    @Override
    public FiretruckWebModel newWM() {
        return new FiretruckWebModel();
    }

    @Override
    public FiretruckWebModel newWM(FiretruckDto dto) {
        return new FiretruckWebModel(dto);
    }
}
