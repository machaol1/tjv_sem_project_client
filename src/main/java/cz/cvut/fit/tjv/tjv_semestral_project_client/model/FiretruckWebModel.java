package cz.cvut.fit.tjv.tjv_semestral_project_client.model;

public class FiretruckWebModel extends FiretruckDto {
    public DepartmentDto department;
    public Long departmentId;

    public FiretruckWebModel() {}

    public FiretruckWebModel(FiretruckDto dto) {
         super(dto);
    }

    public FiretruckWebModel(DepartmentDto department) {
        this.department = department;
    }

    public DepartmentDto getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentDto department) {
        this.department = department;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }
}
