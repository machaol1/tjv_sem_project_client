package cz.cvut.fit.tjv.tjv_semestral_project_client.model;

public class DepartmentDto extends AbstractDtoWithId<Long> {
    private String name;
    private Integer level;

    public DepartmentDto() {}

    public DepartmentDto(DepartmentDto other) {
        id = other.id;
        name = other.name;
        level = other.level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
}
