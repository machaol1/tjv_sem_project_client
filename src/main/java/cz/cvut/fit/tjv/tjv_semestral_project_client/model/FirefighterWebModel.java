package cz.cvut.fit.tjv.tjv_semestral_project_client.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class FirefighterWebModel extends FirefighterDto {
    public DepartmentDto department;
    public final Set<DispatchDto> dispatches;
    public Long departmentId;
    public Set<Long> dispatchIds = new HashSet<>();

    public FirefighterWebModel() {
        dispatches = new HashSet<>();
    }

    public FirefighterWebModel(FirefighterDto dto) {
        super(dto);
        dispatches = new HashSet<>();
    }

    public FirefighterWebModel(FirefighterDto dto, Set<DispatchDto> dispatches) {
        super(dto);
        this.dispatches = dispatches;
    }

    public DepartmentDto getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentDto department) {
        this.department = department;
    }

    public Collection<DispatchDto> getDispatches() {
        return dispatches;
    }

    public void addDispatch(DispatchDto dispatchDto) {
        dispatches.add(dispatchDto);
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public Set<Long> getDispatchIds() {
        return dispatchIds;
    }

    public void addDispatchId(Long id) {
        dispatchIds.add(id);
    }

    public void setDispatchIds(Set<Long> dispatchIds) {
        this.dispatchIds = dispatchIds;
    }
}
