package cz.cvut.fit.tjv.tjv_semestral_project_client.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DispatchWebModel extends DispatchDto {
    public final Set<DepartmentDto> departments;
    public List<Long> departmentIds = new ArrayList<>();
    public final Set<FirefighterDto> attendees;

    public DispatchWebModel() {
        departments = new HashSet<>();
        attendees = new HashSet<>();
    }

    public DispatchWebModel(DispatchDto dto) {
        super(dto);
        departments = new HashSet<>();
        attendees = new HashSet<>();
    }

    public DispatchWebModel(DispatchDto dto, Set<DepartmentDto> departments, Set<FirefighterDto> attendees) {
        super(dto);
        this.departments = departments;
        this.attendees = attendees;
    }

    public Set<DepartmentDto> getDepartments() {
        return departments;
    }

    public Set<FirefighterDto> getAttendees() {
        return attendees;
    }

    public List<Long> getDepartmentIds() {
        return departmentIds;
    }

    public void setDepartmentIds(List<Long> departmentIds) {
        this.departmentIds = departmentIds;
    }
}
