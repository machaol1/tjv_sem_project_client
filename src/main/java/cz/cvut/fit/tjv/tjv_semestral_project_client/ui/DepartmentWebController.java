package cz.cvut.fit.tjv.tjv_semestral_project_client.ui;

import cz.cvut.fit.tjv.tjv_semestral_project_client.data.DepartmentClient;
import cz.cvut.fit.tjv.tjv_semestral_project_client.model.DepartmentDto;
import cz.cvut.fit.tjv.tjv_semestral_project_client.model.DepartmentWebModel;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/department")
public class DepartmentWebController extends AbstractWebController<DepartmentWebModel, DepartmentDto, DepartmentClient, Long> {
    public DepartmentWebController(DepartmentClient departmentClient) {
        super(departmentClient, "department");
    }
}
