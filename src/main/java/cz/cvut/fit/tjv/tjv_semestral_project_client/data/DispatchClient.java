package cz.cvut.fit.tjv.tjv_semestral_project_client.data;

import cz.cvut.fit.tjv.tjv_semestral_project_client.model.DepartmentDto;
import cz.cvut.fit.tjv.tjv_semestral_project_client.model.DispatchDto;
import cz.cvut.fit.tjv.tjv_semestral_project_client.model.DispatchWebModel;
import cz.cvut.fit.tjv.tjv_semestral_project_client.model.FirefighterDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class DispatchClient extends AbstractClient<DispatchWebModel, DispatchDto, Long> {
    protected final String DEP_URI = ONE_URI + "/department";
    protected final String FF_URI = ONE_URI + "/firefighter";

    public DispatchClient (@Value("${backend_url}") String backendUrl) {
        super(backendUrl, "/dispatch", DispatchWebModel.class);
    }

    @Override
    public Mono<DispatchWebModel> readById(Long id) {
        Mono<DispatchDto> dispatchDtoMono = webClient.get()
                .uri(ONE_URI, id)
                .retrieve()
                .bodyToMono(DispatchDto.class);

        Mono<Set<DepartmentDto>> departmentsDto = webClient.get()
                .uri(DEP_URI, id)
                .retrieve()
                .bodyToFlux(DepartmentDto.class)
                .collect(Collectors.toSet()); // Collect the Flux into a Set

        Mono<Set<FirefighterDto>> firefighterDto = webClient.get()
                .uri(FF_URI, id)
                .retrieve()
                .bodyToFlux(FirefighterDto.class)
                .collect(Collectors.toSet());

        // runs in parallel
        return dispatchDtoMono.flatMap(dispatch ->
                Mono.zip(dispatchDtoMono, departmentsDto, firefighterDto)
                        .map(tuple -> new DispatchWebModel(tuple.getT1(), tuple.getT2(), tuple.getT3()))
        );

//        return Mono.zip(dispatchDtoMono, departmentsDto, firefighterDto)
//                .map(tuple -> new DispatchWebModel(tuple.getT1(), tuple.getT2(), tuple.getT3()));
    }

    @Override
    public Mono<DispatchWebModel> create(DispatchWebModel dispatchWebModel) {
        dispatchWebModel.setStart(LocalDateTime.now());

        return super.create(dispatchWebModel)
                .flatMap(dispatch -> Flux.fromIterable(dispatchWebModel.getDepartmentIds())
                        .flatMap(id -> addDepartment(dispatch.id, id))
                        .then(Mono.just(dispatch))
                );
    }

    private Mono<Void> addDepartment(Long id, Long depId) {
        return webClient.put()
                .uri(DEP_URI + "/{depId}", id, depId)
                .retrieve()
                .toBodilessEntity()
                .then()
                ;
    }

    @Override
    public DispatchWebModel newWM() {
        return new DispatchWebModel();
    }

    @Override
    public DispatchWebModel newWM(DispatchDto dto) {
        return new DispatchWebModel(dto);
    }
}
