package cz.cvut.fit.tjv.tjv_semestral_project_client.ui;

import cz.cvut.fit.tjv.tjv_semestral_project_client.data.DepartmentClient;
import cz.cvut.fit.tjv.tjv_semestral_project_client.data.DispatchClient;
import cz.cvut.fit.tjv.tjv_semestral_project_client.model.DispatchDto;
import cz.cvut.fit.tjv.tjv_semestral_project_client.model.DispatchWebModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import reactor.core.publisher.Mono;

@Controller
@RequestMapping("/dispatch")
public class DispatchWebController extends AbstractWebController<DispatchWebModel, DispatchDto, DispatchClient, Long> {
    DepartmentClient departmentClient;

    public DispatchWebController(DepartmentClient departmentClient, DispatchClient client) {
        super(client, "dispatch");
        this.departmentClient = departmentClient;
    }

    @Override
    public Mono<String> showCreate(Model model) {
        return departmentClient.list()
                .collectList()
                .doOnNext(departmentWebModels -> model.addAttribute("departments", departmentWebModels))
                .then(super.showCreate(model))
                .onErrorResume(throwable -> Mono.just("redirect:/error/" + getStatusCode(throwable)))
                ;
    }
}
