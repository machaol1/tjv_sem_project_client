package cz.cvut.fit.tjv.tjv_semestral_project_client.ui;

import cz.cvut.fit.tjv.tjv_semestral_project_client.data.DepartmentClient;
import cz.cvut.fit.tjv.tjv_semestral_project_client.data.FiretruckClient;
import cz.cvut.fit.tjv.tjv_semestral_project_client.model.FiretruckDto;
import cz.cvut.fit.tjv.tjv_semestral_project_client.model.FiretruckWebModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import reactor.core.publisher.Mono;

@Controller
@RequestMapping("/firetruck")
public class FiretruckWebController extends AbstractWebController<FiretruckWebModel, FiretruckDto, FiretruckClient, Long> {
    DepartmentClient departmentClient;

    public FiretruckWebController(DepartmentClient departmentClient, FiretruckClient client) {
        super(client, "firetruck");
        this.departmentClient = departmentClient;
    }

    @Override
    public Mono<String> showCreate(Model model) {
        return departmentClient.list()
                .collectList()
                .doOnNext(departmentWebModels -> model.addAttribute("departments", departmentWebModels))
                .then(super.showCreate(model))
                .onErrorResume(throwable -> Mono.just("redirect:/error/" + getStatusCode(throwable)))
                ;
    }

    @Override
    @GetMapping("/{id}/edit")
    public Mono<String> showEdit(@PathVariable Long id, Model model) {
        return departmentClient.list()
                .collectList()
                .doOnNext(departmentWebModels -> model.addAttribute("departments", departmentWebModels))
                .then(super.showEdit(id, model))
                .onErrorResume(throwable -> Mono.just("redirect:/error/" + getStatusCode(throwable)))
                ;
    }
}
