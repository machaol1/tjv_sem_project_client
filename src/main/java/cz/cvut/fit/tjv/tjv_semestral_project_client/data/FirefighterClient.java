package cz.cvut.fit.tjv.tjv_semestral_project_client.data;

import cz.cvut.fit.tjv.tjv_semestral_project_client.model.DepartmentDto;
import cz.cvut.fit.tjv.tjv_semestral_project_client.model.DispatchDto;
import cz.cvut.fit.tjv.tjv_semestral_project_client.model.FirefighterDto;
import cz.cvut.fit.tjv.tjv_semestral_project_client.model.FirefighterWebModel;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class FirefighterClient extends AbstractClient<FirefighterWebModel, FirefighterDto, Long> {
    protected static final String DEP_URI = ONE_URI + "/department";
    protected static final String DIS_URI = ONE_URI + "/dispatch";
    protected final WebClient departmentWebClient;

    public FirefighterClient (@Value("${backend_url}") String backendUrl) {
        super(backendUrl, "/firefighter", FirefighterWebModel.class);
        departmentWebClient = WebClient.create(backendUrl + "/department");
    }

    public Mono<FirefighterWebModel> showAttend(Long id) {
        Mono<FirefighterWebModel> firefighterMono = webClient.get()
                .uri(ONE_URI, id)
                .retrieve()
                .bodyToMono(FirefighterWebModel.class)
                ;

        Mono<Set<DispatchDto>> dispatchesMono = webClient.get()
                .uri(DIS_URI, id)
                .retrieve()
                .bodyToFlux(DispatchDto.class)
                .collect(Collectors.toSet());

        return Mono.zip(firefighterMono, dispatchesMono)
                .flatMap(tuple -> {
                    FirefighterWebModel firefighter = tuple.getT1();
                    Set<DispatchDto> dispatchDtos = tuple.getT2();

                    dispatchDtos.stream()
                            .map(dispatchDto -> dispatchDto.id)
                            .forEach(firefighter::addDispatchId); // Assuming addDispatchId method exists

                    return Mono.just(firefighter);
                });
    }

    public Mono<Void> attend(Long id, Set<Long> newDisIds) {
        return webClient.get()
                .uri(DIS_URI, id)
                .retrieve()
                .bodyToFlux(DispatchDto.class)
                .collect(Collectors.toSet())
                .flatMap(previousDispatches -> {
                    Set<Long> previousAttendedIds = previousDispatches.stream()
                            .map(dispatch -> dispatch.id)
                            .collect(Collectors.toSet());

                    Set<Long> delListIds = new HashSet<>(previousAttendedIds);
                    Set<Long> addListIds = new HashSet<>(newDisIds);

                    delListIds.removeAll(newDisIds);
                    addListIds.removeAll(previousAttendedIds);

                    Flux<Void> deleteOperations = Flux.fromIterable(delListIds)
                            .flatMap(disId -> webClient.delete()
                                    .uri(DIS_URI + "/{disId}", id, disId)
                                    .retrieve()
                                    .toBodilessEntity()
                                    .then()
                            );

                    Flux<Void> addOperations = Flux.fromIterable(addListIds)
                            .flatMap(disId -> webClient.put()
                                    .uri(DIS_URI + "/{disId}", id, disId)
                                    .retrieve()
                                    .toBodilessEntity()
                                    .then()
                            );

                    return Flux.concat(deleteOperations, addOperations).then();
                });
    }

    @Override
    public Flux<FirefighterWebModel> readAll() {
        return super.readAll().flatMap(this::readDepartment);
    }

    @Override
    public Mono<FirefighterWebModel> create(FirefighterWebModel wm) {
        return departmentWebClient.post() // HTTP POST
                .uri("/{depId}/firefighter", wm.departmentId)
                .contentType(MediaType.APPLICATION_JSON) // set HTTP header
                .bodyValue(wm) // POST data
                .retrieve() // request specification finished
                .bodyToMono(FirefighterWebModel.class) // interpret response body as one element using WM class
                ;
    }

    @Override
    public Mono<Void> edit(Long id, FirefighterWebModel firefighterWebModel) {
        return webClient.put() // HTTP PUT
                .uri(DEP_URI + "/{FFId}", id, firefighterWebModel.departmentId) // update firefighter department
                .retrieve() // request specification finished
                .toBodilessEntity()
                .then(Mono.defer(() -> super.edit(id, firefighterWebModel))) // update firefighter
                ;
    }

    @Override
    protected Mono<FirefighterWebModel> readById(Long id) {
        Mono<FirefighterDto> firefighterMono = webClient.get()
                .uri(ONE_URI, id)
                .retrieve()
                .bodyToMono(FirefighterDto.class);

        Mono<Set<DispatchDto>> dispatchesMono = webClient.get()
                .uri(DIS_URI, id)
                .retrieve()
                .bodyToFlux(DispatchDto.class)
                .collect(Collectors.toSet()); // Collect the Flux into a Set

        return firefighterMono.flatMap(firefighter ->
                dispatchesMono.map(dispatches -> new FirefighterWebModel(firefighter, dispatches))
        ).flatMap(this::readDepartment);
    }

    private Mono<FirefighterWebModel> readDepartment(FirefighterWebModel firefighterWebModel) {
        return webClient.get() // /object
                .uri(DEP_URI, firefighterWebModel.id) // /object/{id}
                .retrieve() // read
                .bodyToMono(DepartmentDto.class) // interpret response body as WM using WM class
                .map(departmentDto -> {
                    firefighterWebModel.setDepartment(departmentDto);
                    return firefighterWebModel;
                })
                ;
    }

    @Override
    public FirefighterWebModel newWM() {
        return new FirefighterWebModel();
    }

    @Override
    public FirefighterWebModel newWM(FirefighterDto dto) {
        return new FirefighterWebModel(dto);
    }
}
