package cz.cvut.fit.tjv.tjv_semestral_project_client.ui;

import cz.cvut.fit.tjv.tjv_semestral_project_client.data.DepartmentClient;
import cz.cvut.fit.tjv.tjv_semestral_project_client.data.DispatchClient;
import cz.cvut.fit.tjv.tjv_semestral_project_client.data.FirefighterClient;
import cz.cvut.fit.tjv.tjv_semestral_project_client.model.FirefighterDto;
import cz.cvut.fit.tjv.tjv_semestral_project_client.model.FirefighterWebModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@Controller
@RequestMapping("/firefighter")
public class FirefighterWebController extends AbstractWebController<FirefighterWebModel, FirefighterDto, FirefighterClient, Long> {
    DepartmentClient departmentClient;
    DispatchClient dispatchClient;

    public FirefighterWebController(DepartmentClient departmentClient, DispatchClient dispatchClient,
                                    FirefighterClient client) {
        super(client, "firefighter");
        this.departmentClient = departmentClient;
        this.dispatchClient = dispatchClient;
    }

    @GetMapping("/{id}/attend")
    public Mono<String> showAttend(@PathVariable Long id, Model model) {
        return client.showAttend(id)
                .flatMap(firefighterWebModel -> {
                    model.addAttribute(url, firefighterWebModel);
                    return dispatchClient.list()
                            .collectList() // Collect all DispatchDto into a List
                            .doOnNext(dispatches -> model.addAttribute("dispatches", dispatches))
                            .then(Mono.just(url + "/attend"));
                })
                .onErrorResume(throwable -> Mono.just("redirect:/error/" + getStatusCode(throwable)));
    }

    @PostMapping("/{id}/attend")
    public Mono<String> attend(@PathVariable Long id, @ModelAttribute FirefighterWebModel wm) {
        return client.attend(id, wm.dispatchIds)
                .then(Mono.just("redirect:/" + url + "/" + id))
                .onErrorResume(throwable -> Mono.just("redirect:/error/" + getStatusCode(throwable)))
                ;
    }

    @Override
    public Mono<String> showCreate(Model model) {
        return departmentClient.list()
                .collectList()
                .doOnNext(departmentWebModels -> model.addAttribute("departments", departmentWebModels))
                .then(super.showCreate(model))
                .onErrorResume(throwable -> Mono.just("redirect:/error/" + getStatusCode(throwable)))
                ;
    }

    @Override
    @GetMapping("/{id}/edit")
    public Mono<String> showEdit(@PathVariable Long id, Model model) {
        return departmentClient.list()
                .collectList()
                .doOnNext(departmentWebModels -> model.addAttribute("departments", departmentWebModels))
                .then(super.showEdit(id, model))
                .onErrorResume(throwable -> Mono.just("redirect:/error/" + getStatusCode(throwable)))
                ;
    }
}
