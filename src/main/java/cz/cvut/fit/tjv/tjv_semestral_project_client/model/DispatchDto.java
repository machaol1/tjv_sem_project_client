package cz.cvut.fit.tjv.tjv_semestral_project_client.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DispatchDto extends AbstractDtoWithId<Long> {
    private Integer dangerLevel;
    private String type;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime start;

    public DispatchDto() {}

    public DispatchDto(DispatchDto other) {
        id = other.id;
        dangerLevel = other.dangerLevel;
        type = other.type;
        start = other.start;
    }

    public Integer getDangerLevel() {
        return dangerLevel;
    }

    public void setDangerLevel(Integer dangerLevel) {
        this.dangerLevel = dangerLevel;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public String printStart() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy hh:mm:ss");
        return start.format(formatter);
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }
}
