package cz.cvut.fit.tjv.tjv_semestral_project_client.ui;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class homeController {
    @GetMapping
    public String homeAction() {
        return "general/home";
    }

    @GetMapping("/error/{errCode}")
    public String errorAction(@PathVariable int errCode, Model model) {
        model.addAttribute("errCode", errCode);
        switch (errCode) {
            case 400:
                model.addAttribute("errMsg", "Tak to nepůjde - buďto se snažíš o něco zakázaného nebo si zadal špatný input");
                break;

            case 404:
                model.addAttribute("errMsg", "Stránka nenalezena (nejspíš si zadal špatné id)");
                break;

            case 500:
                model.addAttribute("errMsg", "Server error - nejspíš je down");
                break;

            default:
                model.addAttribute("errMsg", "Neznámá chyba");
        }
        return "general/error";
    }
}
