package cz.cvut.fit.tjv.tjv_semestral_project_client.model;

public class FiretruckDto extends AbstractDtoWithId<Long> {
    public String name;
    public String type;

    public FiretruckDto() {}

    public FiretruckDto(FiretruckDto other) {
        id = other.id;
        name = other.name;
        type = other.type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
